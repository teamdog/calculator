#coding=utf-8
import sys
import unittest
from PyQt4.QtGui import QApplication
from PyQt4.QtTest import QTest
from PyQt4.QtCore import Qt

from calculator import Calculator

class TestQtGui(unittest.TestCase):

	def setUp(self):

		self.app = QApplication.instance() or QApplication(sys.argv)
		self.w = Calculator()

	def test_addition(self):
		QTest.mouseClick(self.w.digitButtons[3], Qt.LeftButton)
		QTest.mouseClick(self.w.plusButton, Qt.LeftButton)
		QTest.mouseClick(self.w.digitButtons[4], Qt.LeftButton)
		QTest.mouseClick(self.w.equalButton, Qt.LeftButton)
		self.assertEqual(self.w.display.text(), '7.0')

	def test_multiplication(self):
		QTest.mouseClick(self.w.digitButtons[2], Qt.LeftButton)
		QTest.mouseClick(self.w.timesButton, Qt.LeftButton)
		QTest.mouseClick(self.w.digitButtons[9], Qt.LeftButton)
		QTest.mouseClick(self.w.equalButton, Qt.LeftButton)
		self.assertEqual(self.w.display.text(), '18.0')

	def test_pow(self):
		QTest.mouseClick(self.w.digitButtons[5], Qt.LeftButton)
		QTest.mouseClick(self.w.powerButton, Qt.LeftButton)
		QTest.mouseClick(self.w.equalButton, Qt.LeftButton)
		self.assertEqual(self.w.display.text(), '25.0')

	def test_factorial(self):
		QTest.mouseClick(self.w.digitButtons[6], Qt.LeftButton)
		QTest.mouseClick(self.w.factorialButton, Qt.LeftButton)
		QTest.mouseClick(self.w.equalButton, Qt.LeftButton)
		self.assertEqual(self.w.display.text(), '720.0')

	def test_sqrt(self):
		QTest.mouseClick(self.w.digitButtons[9]), Qt.LeftButton)
		QTest.mouseClick(self.w.squareButon, Qt.LeftButton)
		QTest.mouseClick(self.w.equalButton, Qt.LeftButton)
		self.assertEqual(self.w.display.text(), '3.0')	
	
	def test_pow(self):
		QTest.mouseClick(self.w.digitButtons[4], Qt.LeftButton)
		QTest.mouseClick(self.w.powerButton, Qt.LeftButton)
		QTest.mouseClick(self.w.equalButton, Qt.LeftButton)
		self.assertEqual(self.w.display.text(), '16.0')	



suite = unittest.TestLoader().loadTestsFromTestCase(TestQtGui)
unittest.TextTestRunner(verbosity=2).run(suite)